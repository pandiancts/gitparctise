package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;
public class ViewLeadPageFramework extends Annotations{
	
	public ViewLeadPageFramework() {
	       PageFactory.initElements(driver, this);
		} 
	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement leadele;
	public  String verifyFirstName() {
		/*String text = driver.findElementById("viewLead_firstName_sp")
		.getText();*/
		String text = leadele.getText();
		System.out.println(text);
		/*String leadId = driver.findElementById("viewLead_companyName_sp").getText().replaceAll("\\D", "");
		System.out.println(leadId);*/
		
		if(text.equals("Ela")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		
		return text;
	}

}
