package com.leafBot.pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;
public class CreateLeadPageFramework extends Annotations {
	public CreateLeadPageFramework() {
	       PageFactory.initElements(driver, this);
		} 
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement comName;
	public CreateLeadPageFramework typeCompanyName() {
		clearAndType(comName, "CTS");
		/*driver.findElementById("createLeadForm_companyName")
		.sendKeys(cName);*/
		return this;
	}
	
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement firstName;
	public CreateLeadPageFramework typeFirstName() {
		/*driver.findElementById("createLeadForm_firstName")
		.sendKeys(fName);*/
		clearAndType(firstName, "Elamathi");
		return this;
	}
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement lastName;
	public CreateLeadPageFramework typeLastName() {		
		clearAndType(lastName, "Pandian");
		/*driver.findElementById("createLeadForm_lastName")
		.sendKeys(lName);*/
		return this;
	}
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement submit;
	public ViewLeadPageFramework clickCreateLeadButton() {
		/*driver.findElementByClassName("smallSubmit")
		.click();*/
		click(submit);
		/*String leadId = driver.findElementById("viewLead_companyName_sp").getText().replaceAll("\\D", "");
		System.out.println(leadId);*/
		return new ViewLeadPageFramework();
	}
	

}
