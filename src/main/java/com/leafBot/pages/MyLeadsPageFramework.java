package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

public class MyLeadsPageFramework extends Annotations{
	public MyLeadsPageFramework() {
	       PageFactory.initElements(driver, this);
		} 
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement createlead;
	public CreateLeadPageFramework clickCreateLead() {
		//driver.findElementByLinkText("Create Lead")
		//.click();
		click(createlead);
		return new CreateLeadPageFramework();
		
	}

	
}


